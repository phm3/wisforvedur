#!/usr/bin/python
# -*- coding: utf-8 -*-
# TODO: aliast in bashrc
# TODO pass argv to set time interval and maybe even the location ;D
# FIXME, there is a bug with NULL/None values
import requests, os, sys
from xml.etree import ElementTree
import urllib
from datetime import date, datetime, time, timedelta
from pprint import pprint
import platform


os.system('clear') 

location = 1 # Reykjavik
now =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')

justnow = datetime.strptime(now, "%Y-%m-%d %H:%M:%S")
mytime = datetime.strptime(now, "%Y-%m-%d %H:%M:%S")
# set the time range for the upcoming forecast
mytime += timedelta(hours=6)

xmlurl = 'http://xmlweather.vedur.is'

url = xmlurl + '?op_w=xml&type=obs&lang=en&view=xml&ids=' + str(location)

response = requests.get(url, stream=True)
response.raw.decode_content = True
events = ElementTree.iterparse(response.raw)


print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
for elem, event in events:
      
    if event.tag == 'name':
    	print "Location: ", event.text
    	name = event.text

    if event.tag == 'time':
    	print "Time: ", event.text, "UTC"

    if event.tag == 'T':
    	print "Temperature: ", event.text, "°C"
    	temperature = event.text

    if event.tag == 'W':
    	print "Conditions: ", event.text
    	conditions = event.text

    if event.tag == 'F':
    	print "Wind: ", event.text, "m/s", " » " ,int(event.text) * 3.6, "km/h"
    	wind = event.text 
    	


url_update = xmlurl + '?op_w=xml&type=forec&lang=en&view=xml&ids=' + str(location)

response_update = requests.get(url_update, stream=True)   
response_update.raw.decode_content = True
events_update = ElementTree.iterparse(response_update.raw)

print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
print "\n>>>> Upcoming: \n" 


for elem, even in events_update:
    
    if even.tag == 'T':
        
        temp = even.text
       
    if even.tag == 'W':
        condi = even.text

    if even.tag == 'F':
        windu = even.text

    if even.tag == 'ftime':
            
            time = datetime.strptime(even.text,'%Y-%m-%d %H:%M:%S')

            if  time >= justnow and time < mytime:
                print "Time:", time, "UTC"
                print "Temperature: ", temp,  "°C"
                print "Wind: ", windu, "m/s", " » " ,int(windu) * 3.6, "km/h"
                print "Conditions:", condi, "\n"
                #break # the fall
    

print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

# the part for notification on linux, extra check up for mac
if os.name is "posix" and platform.system() == "Linux":
    from gi.repository import Notify
    
    notification_text = (name + "\n" + temperature + " C\n" + str(int(wind) * 3.6) + " km/h\n Conditions: " + str(conditions))
    Notify.init("Weather")

    Hello = Notify.Notification.new("Vedur.is", notification_text)
    Hello.show()
sys.exit()
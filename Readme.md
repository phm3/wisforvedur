# w is for Veður.is

This scripts calls the http://www.vedur.is API (Icelandic Meteorological Office) and retrievs the weather information for selected location. 

## Configure

The time range can be specified by selecting the end point. 

`mytime += timedelta(hours=6)
`

The default location can be also changed:

`location = 1 # Reykjavik`

## Notification

On posix systems, an onscreen notification is available. 


`from gi.repository import Notify`

### Term of use

Don't DDoS vedur.is please.

